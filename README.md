# BMI App

Az alkalmazás BMI index számolását valósítja meg a felhasználó által megadott adatok alapján. Bemutatja az Android alapvető vezérlőit, valamint az egyik legalapvetőbb elrendezés módszerét a LinearLayout-ot.

## Fejlesztőkörnyezet ##

- Android Studio 3.4.1

## Projekt létrehozása ##

- Kiinduló projekt: Empty Activity

- Minimum API level: API 15: Android 4.0.3

- Csomag: gamf.bmiapp


## Alkalmazás felépítése és működése ##

### activity_main.xml ###

Felületet (MainActivity) leíró xml állomány. Vezérlők felületen való elhelyezése **LinearLayout** segítségével valósul meg. **orientation** attribútumát **vertical**-ra állítva a benne elhelyezett vezérlők egymás alatt, sorrendben fognak megjelenni.

Felületen található vezérlők:

- **EditText**: szövegbeviteli mező

- **TextView**: szövegmegjelenítés

- **Button**: nyomógomb

Felületen található szöveges erőforrásokat a **strings.xml**-ben tároljuk kulcs-érték formában.

### MainActivity ###

Az alkalmazás egyetlen komponense, ahol a vezérlők (EditText, TextView, Button) inicializálása és használatbavétele történik. Button kattintására a BMI szöveges értékelését jelenítjük meg egy másik TextView vezérlőn.

**validate** függvénye a felhasználótól érkező bemenetek ellenőrzését valósítja meg. Amennyiben hibás adatokat gépeltek be a felhasználó egy **Toast** üzenetben kap értesítést. **EditText** vezérlőknél csak számok engedélyezettek a vituális billentyűzeten (**inputType** attribútum).

### BMIIndex osztály ###

Alkalmazás logikáját valósítja meg, súly és magasság alapján számolja a BMI index értékét és hozzárendel egy szöveges értékelést, amit a MainActivity jelenít majd meg.