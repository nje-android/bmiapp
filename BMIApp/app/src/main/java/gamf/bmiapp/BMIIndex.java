package gamf.bmiapp;

public class BMIIndex {
    private String[] categories = {
            "Sovány", "Normál testalkatú", "Túlsúlyos", "Erősen túlsúlyos"
    };

    public String calculate(float weight, float height) {
        height /= 100;
        double bmi = weight / (height * height);

        if (bmi < 18.5) {
            return categories[0];
        } else if (bmi >= 18.5 && bmi < 25) {
            return categories[1];
        } else if (bmi >= 25 && bmi < 30) {
            return categories[2];
        } else {
            return categories[3];
        }
    }
}
