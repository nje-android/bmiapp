package gamf.bmiapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText WeightEditText;
    private EditText HeightEditText;
    private Button CalculateButton;
    private TextView ResultTextView;

    private BMIIndex bmiIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        WeightEditText = findViewById(R.id.WeightEditText);
        HeightEditText = findViewById(R.id.HeightEditText);
        CalculateButton = findViewById(R.id.CalculateButton);
        ResultTextView = findViewById(R.id.ResultTextView);

        bmiIndex = new BMIIndex();

        CalculateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (!validate()) {
                        Toast.makeText(getApplicationContext(), "Hibás adatok!", Toast.LENGTH_SHORT).show();
                    }

                    String result = bmiIndex.calculate(Float.parseFloat(WeightEditText.getText().toString()),
                            Float.parseFloat(HeightEditText.getText().toString()));

                    ResultTextView.setText(result);

                } catch (Throwable throwable) {

                }
            }
        });
    }

    private boolean validate() {
        String weightStr = WeightEditText.getText().toString();
        String heightStr = HeightEditText.getText().toString();

        if (weightStr.isEmpty() || heightStr.isEmpty()) {
            return false;
        }

        try {
            Float.parseFloat(weightStr);
            Float.parseFloat(heightStr);
        } catch (Throwable throwable) {
            Log.e("MainActivity", throwable.getMessage());
            return false;
        }

        return true;
    }
}
